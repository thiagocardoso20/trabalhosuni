import java.util.Scanner;

public class Exercicio04 {

	public static void main(String[] args) {
		
		double comp, larg, alt, area;
		int caixas;
		
		Scanner entrada = new Scanner (System.in);
		
		System.out.println("Qual o comprimento da cozinha:  ");
		comp = entrada.nextDouble();
		
		System.out.println("Qual a largura da cozinha: ");
		larg = entrada.nextDouble();
		
		System.out.println("Qual a altura da cozinha: ");
		alt = entrada.nextDouble();
		
		area = (comp * alt * 2) + (larg * alt * 2);
		
		caixas =  (int) (area / 1.5);
		
		System.out.println("Quantidade de caixas de azuleijos para colocar em todas as"
				+ "paredes  "   +   caixas);
		
		entrada.close();
		
	}

}
