import java.util.Scanner;

public class Exercicio03 {

	public static void main(String[] args) {
		
		double pot_lamp, larg_com, comp_com, area_com, pot_total;
		int num_lamp; 
		
		Scanner entrada = new Scanner (System.in);
		
		System.out.println("Digite a potencia da lampada:  ");
		pot_lamp = entrada.nextDouble();
		
		System.out.println("Qual a largura do comodo:  ");
		larg_com = entrada.nextDouble();
		
		System.out.println("Qual o comprimento do comodo:  ");
		comp_com = entrada.nextDouble();
		
		area_com = larg_com * comp_com;
		
		pot_total = area_com * 18;
		
		num_lamp = (int) (pot_total / pot_lamp);
		
		System.out.println("Numero de lampadas necessarias para iluminar esse comodo:  " +  num_lamp);

	    entrada.close();
		
	
	}

}
