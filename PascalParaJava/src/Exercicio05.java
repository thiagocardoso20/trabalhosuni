import java.util.Scanner;

public class Exercicio05 {

	public static void main(String[] args) {
		
		double odom_i, odom_f, litros, valor_t, media, lucro;
		final double gasolina = 1.90;
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Marca��o inicial do odometro(km):  ");
		odom_i = entrada.nextDouble();
		
		System.out.println("Marca��o final do odometro(km):  ");
		odom_f = entrada.nextDouble();
		
		System.out.println("Quantidade de gasolina gasta(litros) ");
		litros = entrada.nextDouble();
		
		System.out.println("Valor total recebido:  ");
		valor_t = entrada.nextDouble();
		
		media = (odom_f - odom_i) / litros;
		
		lucro = valor_t - (litros * gasolina);
		
		System.out.println("Media de consumo em Km/L:  "  + media);
		
		System.out.println("Lucro liquido do dia R$:  "  +  lucro);
        
		
		entrada.close();
		

	}

}
