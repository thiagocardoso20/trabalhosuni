import java.util.Scanner;

public class Exercicio02 {

	public static void main(String[] args) {
		
		double tempC;
		
		Scanner entrada = new Scanner (System.in);
		
		
		
		System.out.println("Informe a temperatura em Fahrenheit: ");
		double tempF = entrada.nextDouble();
		
		tempC = ((tempF - 32) * 5) / 9;
		
		System.out.println("A temperatura em graus Celsius eh:"  + tempC);

		entrada.close();
	}

}
